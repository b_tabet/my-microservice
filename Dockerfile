# Utiliser l'image officielle de Java 17
FROM openjdk:17-jdk-slim

# Définir le répertoire de travail dans le conteneur
WORKDIR /app

# Copier le jar dans le conteneur
COPY target/my-microservice-0.0.1-SNAPSHOT.jar app.jar

# Exposer le port sur lequel l'application va écouter
EXPOSE 8080

# Commande pour exécuter le jar
ENTRYPOINT ["java", "-jar", "/app/app.jar"]
